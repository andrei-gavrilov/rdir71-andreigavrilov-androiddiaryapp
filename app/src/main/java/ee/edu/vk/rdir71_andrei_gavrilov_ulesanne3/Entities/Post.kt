package ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Entities

import android.arch.persistence.room.*
import android.support.annotation.NonNull
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Converters.DateConverter
import java.util.*

@Entity(tableName = "posts", foreignKeys = arrayOf(ForeignKey(
    entity = Country::class,
    parentColumns = arrayOf("code"),
    childColumns = arrayOf("country"))
))
data class Post(
    @PrimaryKey(autoGenerate = true) @NonNull var id: Int,
    var title: String?,
    var description: String?,
    var photo_path: String?,
    var country: String?,
    var place: String?,
    var type: String?,
    var created_at: Date?
)