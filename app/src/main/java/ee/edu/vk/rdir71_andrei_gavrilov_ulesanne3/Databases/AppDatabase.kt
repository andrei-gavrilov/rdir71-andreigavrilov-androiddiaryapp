package ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Databases

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Converters.DateConverter
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.DAOs.CountryDao
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.DAOs.PostDao
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Entities.Country
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Entities.Post
import android.arch.persistence.room.Room
import android.content.Context


@Database(entities = arrayOf(Post::class,Country::class), version = 1)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
    abstract fun countryDao(): CountryDao

    companion object {

        private var sInstance: AppDatabase? = null

        @Synchronized
        fun getInstance(context: Context): AppDatabase {
            if (sInstance == null) {
                sInstance = Room
                    .databaseBuilder(context.applicationContext, AppDatabase::class.java, "diary-db")
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return sInstance!!
        }
    }
}