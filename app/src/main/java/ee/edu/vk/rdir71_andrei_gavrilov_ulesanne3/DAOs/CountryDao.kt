package ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.DAOs

import android.arch.persistence.room.Dao
import android.arch.persistence.room.*
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Entities.Country;

import java.util.List;

@Dao
interface  CountryDao{
    @Query("SELECT * FROM countries")
    fun getAll(): List<Country>

    @Query("SELECT * FROM countries WHERE name LIKE :name")
    fun findByName(name: String): Country

    @Insert
    fun insertAll(vararg countries: Country)

    @Update
    fun update(country: Country)

    @Delete
    fun delete(country: Country)
}
