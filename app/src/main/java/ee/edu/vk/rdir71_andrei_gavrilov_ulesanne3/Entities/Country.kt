package ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "countries")
data class Country constructor(
    @PrimaryKey @NonNull var code: String,
    var name: String?
)
