package ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3

import android.arch.persistence.room.Room
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Databases.AppDatabase
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Entities.Country
import org.jetbrains.anko.doAsync


class MainActivity : AppCompatActivity() {
    private val TAG = "MyActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        doAsync {
            var country=Country("ee","Estonia");
            AppDatabase.getInstance(this@MainActivity).countryDao().insertAll(country)

            for (country in AppDatabase.getInstance(this@MainActivity).countryDao().getAll()) {
                Log.v(TAG, "country =" +country);
            }
        }


    }
}
