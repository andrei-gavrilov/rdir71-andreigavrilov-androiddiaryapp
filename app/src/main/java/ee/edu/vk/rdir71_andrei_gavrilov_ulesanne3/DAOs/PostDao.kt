package ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.DAOs

import android.arch.persistence.room.Dao
import android.arch.persistence.room.*
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Converters.DateConverter
import ee.edu.vk.rdir71_andrei_gavrilov_ulesanne3.Entities.Post

@Dao
interface  PostDao{
    @Query("SELECT * FROM posts")
    fun getAll(): List<Post>

    @Query("SELECT * FROM posts WHERE title LIKE :title")
    fun findByTitle(title: String): Post

    @Insert
    fun insertAll(vararg posts: Post)

    @Update
    fun update(post: Post)

    @Delete
    fun delete(post: Post)
}
